" define plugins to load
call plug#begin('~/.local/share/nvim/site/plugged/')
Plug 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons'
Plug 'lifepillar/vim-mucomplete'
Plug 'itchyny/lightline.vim'
Plug 'shinchu/lightline-gruvbox.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
" Plug 'kien/ctrlp.vim'
"
call plug#end()
"
" " map the leader key to ,
let mapleader=","
"
" " set colorscheme to gruvbox dark
colorscheme gruvbox
set background=dark
"
" " indent line-breaks at the same level as code
set breakindent
"
" " make searching case insensitive
set ignorecase     
" " ... unless the query has capital letters.
set smartcase          
"
" " some formatting options
set showmatch           " Show matching brackets.
set number              " Show the line numbers on the left side.
set formatoptions+=o    " Continue comment marker in new lines.
set expandtab           " Insert spaces when TAB is pressed.
set tabstop=4           " Render TABs using this many spaces.
set shiftwidth=4        " Indentation amount for < and > commands

" " For mucomplete
set completeopt+=menuone,noinsert,noselect 

" " For lightline
set laststatus=2
set noshowmode
" " Gruvbox colors for lightline
let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \   'filetype': 'MyFiletype',
      \   'fileformat': 'MyFileformat',
      \ }
      \ }

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

" add yaml stuffs
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
