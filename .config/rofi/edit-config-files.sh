#!/bin/sh
nl="\n"
declare -a files=(".config/bspwm/bspwmrc" ".config/sxhkd/sxhkdrc" ".bashrc" ".bash_aliases")
file=$(printf "%s\n" "${files[@]}" | rofi -dmenu -p "config file" -font "System San Francisco Display 13" -theme glue_pro_blue)
[[ ! -z $file ]] && st -e vim $HOME/$file
exit 0
